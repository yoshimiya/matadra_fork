package mataDra.dao;

import java.util.ArrayList;
import java.util.List;

import mataDra.entity.Entity;

public abstract class DAO<T> {

	private List<T> entityList = new ArrayList<>();

	public void setEntity(T t) {
		this.entityList.add(t);
	}

	public List<T> getEntity() {
		return this.entityList;
	}

	/**
	 * データ全件取得
	 * @return TODO
	 */
	public abstract List<T> registerAll();

	/**
	 * データ1件取得
	 *
	 * @param indexß
	 * @return
	 */
	public T findByIndex(int index) {
		// 全件データからラムダ式で1件取得
		T result = registerAll().stream().filter(d -> ((Entity) d).getIndex() == index).findFirst().get();
		return result;
	}



}