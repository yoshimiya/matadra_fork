package mataDra.dao.images;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.images.FieldImageEntity;

public class FieldImageDAO extends DAO<FieldImageEntity> {

    @Override
    public List<FieldImageEntity> registerAll() {
        // TODO 自動生成されたメソッド・スタブ
        setEntity(new FieldImageEntity(0, "草原", "〃〃へへ〃〃〃へへ〃〃"));
        setEntity(new FieldImageEntity(1, "荒野", "へ....へ〃..〃....〃へ"));
        setEntity(new FieldImageEntity(2, "魔王城", "霊雲雲闇門闇雲雲雲霊霊"));
        setEntity(new FieldImageEntity(3, "市街", "＝＝＝町門町＝＝＝＝＝"));
		return getEntity();

    }


}
