package mataDra.logics.items;

import mataDra.dao.items.RecoverItemDAO;
import mataDra.entity.items.consume.RecoverItemEntity;

public class RecoverItemListLogic extends ItemListLogic<RecoverItemEntity> {

	@Override
	public RecoverItemEntity getItem(int index) {
		// TODO 自動生成されたメソッド・スタブ

		RecoverItemDAO dao = new RecoverItemDAO();
		return dao.findByIndex(index);
	}

}
