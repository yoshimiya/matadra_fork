package mataDra.logics.images;

import mataDra.dao.images.FieldImageDAO;
import mataDra.logics.DAOLogic;

/**
 * フィールド作成
 *
 * @author ken4310
 *
 */
public class FieldImageLogic extends DAOLogic<FieldImageDAO> {


	//コンストラクタ
	public FieldImageLogic(){
		//登録したデータを読み込む
		setDao(new FieldImageDAO());
		//データの数を記録する
		setCount(getDao().registerAll().size());
	}


	//表示するオブジェクトを返す
	public String convert(int index) {
		// TODO 自動生成されたメソッド・スタブ

		return getDao().findByIndex(index).getImage();
	}


}
