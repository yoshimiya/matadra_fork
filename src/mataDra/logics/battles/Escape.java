package mataDra.logics.battles;

import java.util.List;
import java.util.Random;

import mataDra.entity.creatures.CreatureEntity.BattleStateType;
import mataDra.entity.creatures.Enemy;
import mataDra.entity.creatures.PlayerEntity;
import mataDra.view.ui.MessageArea;

public class Escape {
	private int escapeRate;// 逃走可能率
	private boolean isEscape;

	/**
	 * 逃走成可能率計算
	 *
	 * @param c
	 * @return
	 */
	public int calcEscapePoint(Character c) {
		// 敵の場合は逃走可能率100%、味方は逃走可能率50%;
		if (c instanceof Enemy) {
			this.escapeRate = 100;
		} else {
			this.escapeRate = 50;
		}
		return this.escapeRate;
	}

	/**
	 * 逃走可能判定
	 *
	 * @param c
	 */
	public boolean checkIsEscape(Character c) {
		int r = new java.util.Random().nextInt(99);
		Random random = new Random();
		random.nextInt();
		int point = calcEscapePoint(c);
		// 乱数が逃走成功率以内ならば逃走成功、以上の場合は逃走失敗。
		this.isEscape = (point <= r) ? true : false;
		return this.isEscape;
	}

	/**
	 * 味方全体の逃走処理
	 *
	 * @param ps
	 * @param eventBattle
	 */
	public void usePlayerEscape(List<PlayerEntity> ps, boolean eventBattle) {
		boolean escape;
		PlayerEntity leader = ps.get(0);
		String msg;
		// イベントバトルは逃走不可能
		if (eventBattle) {
			escape = false;
		} else {
			escape = checkIsEscape(leader);
		}

		// 逃走時メッセージ
		msg = leader.getName() + "たちはにげだした。";
		MessageArea.message(msg);
		// 逃走成功の場合プレイヤー全員逃走済みに変更
		if (escape) {
			for (PlayerEntity p : ps) {
				p.setBattleStateType(BattleStateType.escaped);
			}
		} else {
			// 逃走失敗の場合、プレイヤー全員行動不可
			for (PlayerEntity p : ps) {
				p.setBattleStateType(BattleStateType.immovable);
				msg = "しかしまわりこまれてしまった。";
				MessageArea.message(msg);
			}

		}
	}

	/**
	 * 敵1体の逃走処理
	 *
	 * @param e
	 */
	public void useEnemyEscape(Enemy e) {
		e.setBattleStateType(BattleStateType.escaped);
		String msg = e.getName() + "はにげだした。";
		MessageArea.message(msg);

	}
	/**逃走状態リセット
	 * @param cs
	 */
	public void resetEscape(List<Character> cs){
		BattleStateType bs;
		for(Character c : cs){
			bs = c.getBattleStateType();
			//逃走状態のキャラクタを初期値に戻す
			if(bs.equals(BattleStateType.escaped)){
				c.setBattleStateType(BattleStateType.movable);
			}
		}
	}

}
