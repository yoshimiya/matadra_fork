package mataDra.logics.battles;

import java.util.List;

import mataDra.entity.creatures.Character;
import mataDra.entity.creatures.PlayerEntity;
import mataDra.entity.creatures.Character.BattleStateType;
import mataDra.entity.sounds.BattleSound01;
import mataDra.entity.sounds.BattleSound02;
import mataDra.entity.sounds.BattleSound05;
import mataDra.entity.sounds.BattleSound06;
import mataDra.entity.sounds.SoundEntity;
import mataDra.view.ui.MessageArea;
import mataDra.viewss.ui.SoundPlayer;

public abstract class AttackLogic {
    /**
     * ボーナスステータス
     *
     * @param status
     *            呪文の場合intelligence、スキルの場合technique
     * @return
     */
    public int getBonus(int status) {
        double r = Math.random();
        int bonus = (int) (status * 1.0 * r);
        return bonus;
    }

    /**
     * 総合攻撃力
     *
     * @param status
     *            呪文の場合はintelligence、それ以外はstrength
     * @param bonus
     * @param point
     * @return attack
     */
    public int calcTotalAttack(int status, int bonus, int point) {
        int attack = status + bonus + point;
        // 総合攻撃力を255までに抑える。
        if (attack > 255) {
            attack = 255;
        }
        return attack;
    }

    /**
     * 総合防御力
     *
     * @param status
     *            呪文の場合intelligenceそれ以外はvitality
     * @param armorPoint
     *            呪文の場合0
     * @return defence
     */
    public int calcTotalDefence(int status, int armorPoint) {
        int defence = status + armorPoint;
        // 総合防御力を255までに抑える
        if (defence > 255) {
            defence = 255;
        }
        return defence;
    }

    /**
     * 最終命中率
     *
     * @param offenceStatus
     *            魔法の場合intelligenceそれ以外はagility
     * @param defenceStatus
     *            魔法の場合intelligenceそれ以外はagility
     * @return 命中率
     */
    public int calcAttackHitRate(int offenceStatus, int defenceStatus) {
        // 命中率数値パーセント換算
        int hit = 100 + (offenceStatus - defenceStatus);
        // 命中率を最大100％に抑える
        if (hit > 100) {
            hit = 100;
        } else if (hit < 0) {
            hit = 0;
        }
        return hit;

    }

    /**
     * 総合ダメージ
     *
     * @param attack
     * @param defence
     * @return ダメージ
     */
    public int calcTotalDamage(int attack, int defence) {
        int damage = attack - defence;
        if (damage < 0) {
            damage = 0;
        }
        return damage;
    }

    /**
     * 攻撃ミス判定
     *
     * @param hit
     * @return boolean
     */
    public boolean isAttackMiss(int hit) {
        int r = new java.util.Random().nextInt(99);
        boolean isMiss = false;
        if (hit > r) {
            isMiss = true;
        } else {
            isMiss = false;
        }
        return isMiss;
    }

    /**
     * 攻撃減衰率 パーティの人数が多いほど攻撃力が減少。
     *
     * @param partyIndex
     * @return
     */
    public double calcDampingRate(int partyIndex) {
        double attackRate = (1.0 - (0.1 * (partyIndex + 1.0)));
        return attackRate;
    }

    /**
     * クリティカル被ダメージ時処理とメッセージ
     *
     * @param dc
     * @param damage
     * @param isMiss
     * @param isCritical
     * @return
     */
    public void CriticalDamageMessage(Character oc, Character dc) {
        String msg = "";
        // プレイヤー・エネミー用クリティカル音を設定
        SoundEntity[] sounds = new SoundEntity[2];
        sounds[0] = new BattleSound01();
        sounds[1] = new BattleSound02();

        // クリティカル時、防御力無視でダメージ二倍
        int damage = oc.getStrength() + oc.getWeapon().getPoint() * 2;

        // 残りHPが0以下の場合、現在のHP全てを喪失HPとする三項演算子。
        int lostHp = (dc.getHp() - damage) < 0 ? dc.getHp() : (dc.getHp() - damage);
        dc.setHp(lostHp);

        // 受け手がプレイヤーの場合、クリティカルメッセージ変更
        if (dc instanceof PlayerEntity) {
            msg = "つうこんのいちげき!";
            SoundPlayer.play(sounds[0]);

        } else {
            msg = "かいしんのいちげき!";
            SoundPlayer.play(sounds[1]);
        }

        MessageArea.message(msg);
        msg = dc.getName() + "は" + damage + "ポイントのダメージをdamaged";
        if (dc instanceof PlayerEntity) {
            msg.replaceFirst("damaged", "をうけた!");
        } else {
            msg.replaceFirst("damaged", "をあたえた!");
        }
        MessageArea.message(msg);
    }

    /**
     * 通常被ダメージ時処理とメッセージ
     *
     * @param dc
     * @param damage
     * @param isMiss
     * @param isCritical
     * @return
     */
    public void AttackDamageMessage(Character dc, int damage, boolean isMiss) {
        String msg = "";
        // プレイヤー・エネミー用戦闘音を設定
        SoundEntity[] sounds = new SoundEntity[2];
        sounds[0] = new BattleSound05();
        sounds[1] = new BattleSound06();
        // 残りHPが0以下の場合、現在のHP全てを喪失HPとする三項演算子。
        int lostHp = (dc.getHp() - damage) < 0 ? dc.getHp() : (dc.getHp() - damage);
        // ダメージ反映
        // ミスの場合
        if (isMiss) {
            msg = dc.getName() + "はすばやくみをかわした!";
            SoundPlayer.play(sounds[0]);
            MessageArea.message(msg);

        } else if (damage <= 0) {
            // 攻撃無効
            msg = "ミス!" + dc.getName() + "はダメージをうけない!";
            SoundPlayer.play(sounds[1]);
            MessageArea.message(msg);
        } else {
            // 攻撃確定
            dc.setHp(lostHp);
            msg = dc.getName() + "は" + damage + "ポイントのダメージをdamaged";
            // プレイヤーの場合、被ダメージメッセージ変更
            if (dc instanceof PlayerEntity)
                msg.replaceFirst("damaged", "うけた!");
            else {
                msg.replaceFirst("damaged", "あたえた!");
            }
        }
        MessageArea.message(msg);
    }

    /**
     * キャラクター死亡処理
     *
     * @param dc
     * @return 死亡メッセージ
     */
    public void deadMessage(Character dc) {
        String msg = "";
        if (dc.getHp() <= 0) {
            // 生存フラグを変更
            dc.setBattleStateType(BattleStateType.dead);
            msg = dc.getName() + "dead";
            if (dc instanceof PlayerEntity) {
                msg = dc.getName() + "しんでしまった";
            } else {
                msg = dc.getName() + "をたおした";
            }
        }
        MessageArea.message(msg);
    }

    /**
     * 最終クリティカル率
     *
     * @param offenceTechnique
     * @param bonus
     * @param defenceTechnique
     * @return クリティカル率
     */
    public int calcCriticalRate(int offenceTechnique, int bonus, int defenceTechnique) {
        int critical = (offenceTechnique + bonus) - defenceTechnique;
        // クリティカル率を最大50％、最低1%に抑える
        if (critical > 50) {
            critical = 50;
        } else if (critical < 1) {
            critical = 1;
        } else {

        }
        return critical;

    }

    /**
     * クリティカル判定 通常攻撃でのみ使用
     *
     * @param critical
     * @return boolean
     */
    public boolean isAttackCritical(int critical) {
        int r = new java.util.Random().nextInt(99);
        boolean isCritical = false;
        // 1~100までの乱数とクリティカル率を比較
        if (critical > r) {
            isCritical = true;
        } else {
            isCritical = false;
        }
        return isCritical;
    }

    /**
     * 攻撃設定
     *
     * @param offenceCharacter
     * @param abilityIndex
     * @param defenceCharacter
     * @param partyIndex
     */
    public abstract void setAttack(Character oc, int abilityIndex, List<Character> dcs);

}
