package mataDra.logics.ui;

/**
 * ゲーム内メッセージのオブジェクト
 *
 *
 */
public class MessageLogic {
	private int textMaxLength =20;//1行の文字数

    /**
     * メッセージの末尾の余白を全角スペースで埋める
     *
     * @param text
     * @param frameWidth
     * @return
     */
    public String spacePadding(String text) {
        // 最大文字数-メッセージの文字数=余白数
        int len = this.textMaxLength - text.length();
        StringBuilder sb = new StringBuilder();
        // メッセージの文字数が最大数の場合、そのままメッセージを返す。
        if (len == 0) {
            return text;

        } else {
            sb.append(text);
            // 最大文字数分全角スペースを追加
            for (int i = 0; i < len; i++) {
                sb.append("　");
            }

            return sb.toString();
        }
    }

    /**
     * 文字列を改行記号記号、または最大文字数で折り返す。
     *
     * @param text
     * @param frameWidth
     * @return
     */
    public String[] separate(String text) {
        // 文章の改行ごとに文字列配列に格納(1回目)
        String[] textList = text.split("\n", 0);
        StringBuilder sb = new StringBuilder();
        for (String s : textList) {
            // フレームの最大文字数おきに正規表現で改行を追加
            String regex = "(.{" + this.textMaxLength + "})";
            String tmpText = s.replaceAll(regex, "$1\n");
            // 一度文字列を結合
            sb.append(tmpText);
            sb.append("\n");
        }
        // 文章の改行ごとに文字列配列に格納(2回目)
        String[] wrapText = sb.toString().split("\n", 0);

        // 文字列の末尾に最大文字数分余白を追加
        for (int i = 0; i < wrapText.length; i++) {
            wrapText[i] = spacePadding(wrapText[i]);
        }

        return wrapText;
    }

    //
    /**
     * 半角英数字メッセージを全角文字に変換する
     *
     * @param text
     * @return chars
     */
    public char[] convert(String text) {
        char[] chars = text.toCharArray();
        // 1文字ずつテキストを文字型配列に格納
        for (int i = 0; i < chars.length; i++) {

            // 半角英数字記号を全角英数字に変換
            // (全角、半角の違いに注意)
            if ('a' <= chars[i] && chars[i] <= 'z') {
                chars[i] = (char) (chars[i] - 'a' + 'ａ');
            } else if ('A' <= chars[i] && chars[i] <= 'Z') {
                chars[i] = (char) (chars[i] - 'A' + 'Ａ');
            } else if ('0' <= chars[i] && chars[i] <= '9') {
                chars[i] = (char) (chars[i] - '0' + '０');
            } else if('!' == chars[i]){
                chars[i] = '！';
            }else if('?' == chars[i]){
                chars[i] = '？';
            }
        }
        return chars;
    }


}
