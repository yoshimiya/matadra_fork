package mataDra.logics.ui;

/**
 * メッセージ外枠表示クラス
 *
 *
 */
public abstract class FrameLogic {
    // 外枠の模様となる文字
    private String border;
    private String borderBottom;

    /**
     * メッセージ外枠(上下部)の作成
     *
     */
    public abstract void drawUpBorder(String text);
    public abstract void drawBottomBorder(String text);

    /**
     * 外枠左線の作成
     *
     */
    public abstract void drawLeftBorder();

    /**
     * 外枠右線の作成
     *
     */
    public abstract void drawRightBorder();

    // ゲッターセッター自動生成
	public String getBorder() {
		return border;
	}
	public void setBorder(String border) {
		this.border = border;
	}
	public String getBorderBottom() {
		return borderBottom;
	}
	public void setBorderBottom(String borderBottom) {
		this.borderBottom = borderBottom;
	}




}
