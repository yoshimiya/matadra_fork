package mataDra.view.ui;

import mataDra.logics.ui.MessageLogic;
import mataDra.logics.ui.WaitLogic;

public class EffectImageArea {
	public void show(String text,int waitNum){
		 //メッセージ用インスタンス
       MessageLogic messageLogic = new MessageLogic();
       // ウェイト用インスタンス
       WaitLogic waitLogic = new WaitLogic();

           // 文字列を一文字ずつ全角文字に変換
           char[] chars = messageLogic.convert(text);

           for (char c : chars) {

               System.out.print(c);
               if (c != '　') {
                   waitLogic.wait(waitNum);
               }
           }

       waitLogic.wait(3);
   }



}
