package mataDra.entity.creatures;

import java.util.List;

import mataDra.entity.ability.AbilityEntity;

public class EnemyEntity extends CreatureEntity {
	private String suffix;//接尾辞
	private int imageIndex;//画像番号


	/**コンストラクタ自動生成
	 * @param index
	 * @param name
	 * @param hp
	 * @param charge
	 * @param attack
	 * @param experience
	 * @param level
	 * @param abilities
	 * @param battleStateType
	 * @param attribute
	 * @param suffix
	 * @param imageIndex
	 */
	public EnemyEntity(int index, String name, int hp, int charge, int attack, int experience, int level,
			List<AbilityEntity> abilities, BattleStateType battleStateType, Attribute attribute, String suffix,
			int imageIndex) {
		super(index, name, hp, charge, attack, experience, level, abilities, battleStateType, attribute);
		this.suffix = suffix;
		this.imageIndex = imageIndex;
	}


	//ゲッターセッター自動生成
	public String getSuffix() {
		return suffix;
	}


	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}


	public int getImageIndex() {
		return imageIndex;
	}


	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}


	//ToString自動生成
	@Override
	public String toString() {
		return "EnemyEntity [suffix=" + suffix + ", imageIndex=" + imageIndex + ", getSuffix()=" + getSuffix()
				+ ", getImageIndex()=" + getImageIndex() + ", getName()=" + getName() + ", getHp()=" + getHp()
				+ ", getCharge()=" + getCharge() + ", getAttack()=" + getAttack() + ", getExperience()="
				+ getExperience() + ", getLevel()=" + getLevel() + ", getAbilities()=" + getAbilities()
				+ ", getBattleStateType()=" + getBattleStateType() + ", getAttribute()=" + getAttribute()
				+ ", getIndex()=" + getIndex() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}





}
