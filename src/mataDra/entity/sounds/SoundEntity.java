package mataDra.entity.sounds;

import mataDra.entity.Entity;

public class SoundEntity extends Entity {

	public enum SoundType{
		PLAYER_HIT, ENEMY_HIT, PLAYER_CRITICAL, ENEMY_CRITICAL, MISS, NO_DAMAGE, ASSASINATE,
		  LEVEL_UP, INN,
		  FIELD,TOWN,BATTLE
	};

	private String name;
	private String soundEffect;
	private SoundType soundType;


	/**コンストラクタ自動生成
	 * @param index
	 * @param name
	 * @param soundEffect
	 * @param soundType
	 */
	public SoundEntity(int index, String name, String soundEffect, SoundType soundType) {
		super(index);
		this.name = name;
		this.soundEffect = soundEffect;
		this.soundType = soundType;
	}

	//ゲッターセッター自動生成
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSoundEffect() {
		return soundEffect;
	}


	public void setSoundEffect(String soundEffect) {
		this.soundEffect = soundEffect;
	}


	public SoundType getSoundType() {
		return soundType;
	}


	public void setSoundType(SoundType soundType) {
		this.soundType = soundType;
	}

	//ToString自動生成
	@Override
	public String toString() {
		return "SoundEntity [name=" + name + ", soundEffect=" + soundEffect + ", soundType=" + soundType
				+ ", getName()=" + getName() + ", getSoundEffect()=" + getSoundEffect() + ", getSoundType()="
				+ getSoundType() + ", getIndex()=" + getIndex() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}







}