package mataDra.main.test;

import mataDra.logics.sounds.SoundLogic;
import mataDra.view.ui.SoundArea;

public class SoundTest {
	public static void main(String[] args) {

		SoundLogic soundLogic = new SoundLogic();
		SoundArea soundArea = new SoundArea();

		//インデックスをすべて再生
		for(int i=0;i<20; i++){
			soundArea.show(soundLogic.exec(i), 1);

		}

	}

}
